View README.html for a more elaborate project description.

USAGE (Unix Environment)
------------------------

chmod 755 gradlew generate.sh
./gradlew clean
./gradlew makeJar
./generate.sh <path/to/dataFile> <type>

* <path/to/dataFile> can be either absolute or relative.
* <type> is either 'linear' or 'cubic'.