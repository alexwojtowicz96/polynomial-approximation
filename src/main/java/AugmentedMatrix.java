/**
 * An AugmentedMatrix is an NxN matrix augmented with an Nx1 matrix. Augmented Matrices can be
 * manipulated using row operations found in the GaussOps class.
 * 
 * @author  Alexander Wojtowicz
 * @version 1.0
 * @since   2019-04-11
 */

import java.math.BigDecimal;
import java.util.Vector;

public class AugmentedMatrix {
	
	private Vector<Vector<BigDecimal>> matrix;
//=================================================================================================
	
	/**
	 * Given two matrices, create an augmented matrix.
	 * If the preconditions aren't satisfied, AugmentedMatrix::matrix will be empty.
	 * 
	 * @param m1  An NxN matrix
	 * @param m2  An Nx1 matrix
	 */
	public AugmentedMatrix(Matrix m1, Matrix m2) {
		this.matrix = new Vector<Vector<BigDecimal>>();
		if(m1.size() == m1.getRow(0).size() && m1.size() == m2.size() && m2.getRow(0).size() == 1) {
			for(int i=0; i<m1.size(); i++) {
				Vector<BigDecimal> row = m1.getRow(i);
				row.add(m2.get(i, 0));
				this.matrix.add(row);
			}
		}
		else {
			System.out.println("Error:  Could not create AugmentedMatrix. Incompatible dimensions!");
		}
	}
//=================================================================================================
	
	/**
	 * String representation of the (formatted) AugmentedMatrix.
	 * 
	 * @return string representation of the AugmentedMatrix
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		Vector<Vector<BigDecimal>> tMatrix = MatrixOps.transpose(this.matrix);
		int[] maxColLens = new int[tMatrix.size()];
		
		for(int i=0; i<tMatrix.size(); i++) {
			int max = 0;
			for(int j=0; j<tMatrix.get(i).size(); j++) {
				String elem = new Double(tMatrix.get(i).get(j).doubleValue()).toString();
				if(elem.length() > max) {
					max = elem.length();
				}
			}
			maxColLens[i] = max;
		}
		
		for(int i=0; i<this.matrix.size(); i++) {
			for(int j=0; j<this.matrix.get(i).size(); j++) {
				if(j != 0) {
					builder.append("  ");
				}
				if(j == this.matrix.get(i).size() - 1) {
					builder.append("|  ");
				}
				String elem = new Double(this.matrix.get(i).get(j).doubleValue()).toString();
				int numSpaces = maxColLens[j] - elem.length();
				for(int k=0; k<numSpaces; k++) {
					builder.append(" ");
				}
				builder.append(this.matrix.get(i).get(j).doubleValue());
			}
			builder.append("\n");
		}
		
		return new String(builder);
	}
//=================================================================================================
	
	/**
	 * Returns the Vector<BigDecimal> at some row in the AugmentedMatrix::matrix.
	 * 
	 * @param row index of the matrix
	 * 
	 * @return the row at the given index
	 */
	public Vector<BigDecimal> get(int index) {
		return this.matrix.get(index);
	}
//=================================================================================================
	
	/**
	 * Returns the number of rows in the AugmentedMatrix::matrix.
	 * 
	 * @return size of the row Vector<Vector<BigDecimal>>
	 */
	public int size() {
		return this.matrix.size();
	}
//=================================================================================================
	
	/**
	 * Adds a row into the AugmentedMatrix::matrix at some index.
	 * 
	 * @param index of a row
	 * @param a Vector<BigDecimal> (a row)
	 */
	public void add(int index, Vector<BigDecimal> row) {
		this.matrix.add(index, row);
	}
//=================================================================================================
	
	/**
	 * Removes a row at some index in the AugmentedMatrix::matrix.
	 * 
	 * @param index of a row
	 */
	public void remove(int index) {
		this.matrix.remove(index);
	}
//=================================================================================================
	
	/**
	 * Gets the AugmentedMatrix::matrix.
	 * 
	 * @return AugmentedMatrix::matrix
	 */
	public Vector<Vector<BigDecimal>> getMatrix() {
		return this.matrix;
	}
//=================================================================================================
	
	/**
	 * Sets the value at some index (rowIndex x colIndex).
	 * 
	 * @param rowIndex
	 * @param colIndex
	 * @param value
	 */
	public void setValue(int rowIndex, int colIndex, BigDecimal value) {
		this.matrix.get(rowIndex).remove(colIndex);
		this.matrix.get(rowIndex).add(colIndex, value);
	}
//=================================================================================================
	
	/**
	 * Get a list of values in the b matrix. The position refers to the row index.
	 * 
	 * @return Vector<BigDecimal> bMatrix values in the b matrix
	 */
	public Vector<BigDecimal> getBValues() {
		Vector<BigDecimal> bMatrix = new Vector<BigDecimal>();
		for(int i=0; i<this.matrix.size(); i++) {
			bMatrix.add(this.matrix.get(i).get(this.matrix.get(i).size()-1));
		}
		
		return bMatrix;
	}
//=================================================================================================
	
	/**
	 * Get the A matrix
	 * 
	 * @return new Matrix(A), where A is Vector<Vector<BigDecimal>>
	 */
	public Matrix getAMatrix() {
		Vector<Vector<BigDecimal>> aMat = new Vector<Vector<BigDecimal>>();
		for(int i=0; i<this.matrix.size(); i++) {
			Vector<BigDecimal> row = new Vector<BigDecimal>();
			for(int j=0; j<this.matrix.get(i).size()-1; j++) {
				row.add(this.matrix.get(i).get(j));
			}
			aMat.add(row);
		}
		
		return new Matrix(aMat);
	}
//=================================================================================================
	
}
