/** 
 * A Grapher is an object which parses data to be used in a graph which it creates from that data.
 * 
 * @author  Alexander Wojtowicz
 * @version 1.0
 * @since   2019-04-26
 */

import java.awt.Color;
import java.awt.Dimension;
import java.awt.geom.Point2D;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Scanner;
import java.util.Vector;
import javax.swing.JFrame;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.util.ShapeUtilities;

public class Grapher {
	
	private Vector<Point2D.Double> points;                     
	private LinkedHashMap<Vector<BigDecimal>, Double[]> leastSquaresEqs;  
	private LinkedHashMap<Vector<BigDecimal>, Double[]> interpolationEqs;  
//=================================================================================================
	
	/**
	 * Creates a Grapher with a single output file of equations and a set of points.
	 * 
	 * @param eqs     File containing the equations
	 * @param points  File containing the data set in the same directory as the equation file(s)
	 * 
	 * @throws IOException if files are not readable
	 * @throws NumberFormatException if strings cannot be parsed as numbers
	 */
	public Grapher(File eqs, File points) throws NumberFormatException, IOException {
		this.points = new Vector<Point2D.Double>();
		this.leastSquaresEqs = new LinkedHashMap<Vector<BigDecimal>, Double[]>();
		this.interpolationEqs = new LinkedHashMap<Vector<BigDecimal>, Double[]>();
		
		String colVal = new Character(eqs.getCanonicalPath().toCharArray()
				[eqs.getCanonicalPath().length()-5]).toString();
		int yCol = Integer.parseInt(colVal) + 1;
				
		Scanner scanner = new Scanner(points);
		int counter = 0;
		while(scanner.hasNextLine()) {
			if(counter > 1) {
				String line = scanner.nextLine();
				line = line.replace('|', ' ');
				Point2D.Double point = new Point2D.Double(
						new Double(Double.parseDouble(line.split("\\s+")[0])), 
						new Double(Double.parseDouble(line.split("\\s+")[yCol])));
				this.points.add(point);
			}
			else {
				scanner.nextLine();
			}
			counter++;
		}
		scanner.close();
		scanner = new Scanner(eqs);
		while(scanner.hasNextLine()) {
			String[] elems = scanner.nextLine().split(";\\s+");
			
			// Determine x domain...
			Double[] domain = new Double[2];
			domain[0] = Double.parseDouble(elems[0].split("\\s+")[0]);
			domain[1] = Double.parseDouble(elems[0].split("\\s+")[elems[0].split("\\s+").length-1]);
			
			// Convert equation to raw coefficients in ascending x order...
			Vector<BigDecimal> eqCoeffs = new Vector<BigDecimal>();
			String[] pieces = elems[1].split("\\s+");
			Vector<String> desired = new Vector<String>();
			for(int i=0; i<pieces.length; i++) {
				if(i % 2 == 0 && i > 1) {
					desired.add(pieces[i]);
				}
			}
			eqCoeffs = Grapher.convertEqStringsToDoubles(desired);
			
			// Add to appropriate map of equations
			if(elems[2].equals("least-squares")) {
				this.leastSquaresEqs.put(eqCoeffs, domain);
			}
			else if(elems[2].equals("interpolation")) {
				this.interpolationEqs.put(eqCoeffs, domain);
			}
		}
		scanner.close();
	}
//=================================================================================================
	
	/**
	 * Find the x-value in the domain of all equations which is the lowest.
	 * 
	 * @return minimum x-value in the domain
	 */
	public Double getDomainMin() {
		Double min = new Double(Double.MAX_VALUE);
		for(int i=0; i<this.points.size(); i++) {
			if(this.points.get(i).x < min) {
				min = this.points.get(i).x;
			}
		}
		
		return min;
	}
//=================================================================================================
	
	/**
	 * Find the x-value in the domain of all equation which is the highest.
	 * 
	 * @return maximum x-value in the domain
	 */
	public Double getDomainMax() {
		Double max = new Double(Double.MIN_VALUE);
		for(int i=0; i<this.points.size(); i++) {
			if(this.points.get(i).x > max) {
				max = this.points.get(i).x;
			}
		}
		
		return max;
	}
//=================================================================================================
	
	/**
	 * Finds the minimum y-value in all the points.
	 * 
	 * @return minimum y-value
	 */
	public Double getRangeMin() {
		Double min = Double.MAX_VALUE;
		for(int i=0; i<this.points.size(); i++) {
			if(this.points.get(i).y < min) {
				min = this.points.get(i).y;
			}
		}
		
		return min;
	}
//=================================================================================================
	
	/**
	 * Finds the maximum y-value in all the points.
	 * 
	 * @return maximum y-value
	 */
	public Double getRangeMax() {
		Double max = Double.MIN_VALUE;
		for(int i=0; i<this.points.size(); i++) {
			if(this.points.get(i).y > max) {
				max = this.points.get(i).y;
			}
		}
		
		return max;
	}
//=================================================================================================
	
	/**
	 * Returns all points contained within this Grapher.
	 * 
	 * @return this.points
	 */
	public Vector<Point2D.Double> getPoints() {
		return this.points;
	}
//=================================================================================================
	
	/**
	 * Returns a vector of all least-squares EQ strings along with the domain over which the
	 * equation is valid (domain[0] < x <= domain[1]).
	 * 
	 * @return this.leastSquaresEqs
	 */
	public LinkedHashMap<Vector<BigDecimal>, Double[]> getLeastSquaresEqs() {
		return this.leastSquaresEqs;
	}
//=================================================================================================
	
	/**
	 * Returns a vector of all interpolation EQ strings along with the domain over which the
	 * equation is valid (domain[0] < x <= domain[1]).
	 * 
	 * @return this.interpolationEqs
	 */
	public LinkedHashMap<Vector<BigDecimal>, Double[]> getInterpolationEqs() {
		return this.interpolationEqs;
	}
//=================================================================================================
	
	/**
	 * Finds a set of (x, y) coordinates to simulate the curve of any particular polynomial function.
	 * The polynomial equation is in the form:  c0 + c1x + c2x^2 + c3x^3 + ... + cN-1^N-1
	 * 
	 * @param xMin      Initial x
	 * @param xMax      Final x
	 * @param coeffs    c0, c1, c2, c3, ..., cN-1
	 * @param interval  Time between each x value) [Must be positive!]
	 * 
	 * @return A vector of all (x, y) coordinates to simulate the curve
	 */
	public static Vector<Point2D.Double> findCoordinatesForPolynomial(
			Double xMin, Double xMax, BigDecimal[] coeffs, Double interval) {
		Vector<Point2D.Double> points = new Vector<Point2D.Double>();
		Double x = xMin;
		
		while(x <= xMax) {
			BigDecimal[] vals = new BigDecimal[coeffs.length];
			for(int i=coeffs.length-1; i>-1; i--) {
				vals[i] = coeffs[i];
				for(int j=0; j<i; j++) {
					vals[i] = vals[i].multiply(new BigDecimal(x));
				}
			}
			BigDecimal yValue = new BigDecimal(0);
			for(int i=0; i<vals.length; i++) {
				yValue = yValue.add(vals[i]);
			}
			points.add(new Point2D.Double(x, yValue.doubleValue()));
			
			x += interval;
		}
		if(!new Double(points.get(points.size()-1).x).equals(xMax)) {
			x = xMax;
			BigDecimal[] vals = new BigDecimal[coeffs.length];
			for(int i=coeffs.length-1; i>-1; i--) {
				vals[i] = coeffs[i];
				for(int j=0; j<i; j++) {
					vals[i] = vals[i].multiply(new BigDecimal(x));
				}
			}
			BigDecimal yValue = new BigDecimal(0);
			for(int i=0; i<vals.length; i++) {
				yValue = yValue.add(vals[i]);
			}
			points.add(new Point2D.Double(x, yValue.doubleValue()));
		}
		
		return points;
	}
//=================================================================================================
	
	/**
	 * Converts equations in string form back into BigDecimal format (based on double values).
	 * 
	 * @param inputStrings  A vector of equation strings
	 * 
	 * @return eqCoeffs  All coefficients stripped from the equations (in the same order)
	 */
	public static Vector<BigDecimal> convertEqStringsToDoubles(Vector<String> inputStrings) {
		Vector<BigDecimal> eqCoeffs = new Vector<BigDecimal>();
		for(int i=0; i<inputStrings.size(); i++) {
			char[] arr = inputStrings.get(i).toCharArray();
			StringBuilder coeff = new StringBuilder();
			for(int j=0; j<arr.length; j++) {
				if(arr[j] == 'x') {
					break;
				}
				else if(arr[j] == '*') {
					coeff.append("E");
					boolean write = false;
					for(int k=j; k<arr.length; k++) {
						if(write) {
							if(arr[k] != ')') {
								coeff.append(arr[k]);
							}
							else {
								break;
							}
						}
						else if(arr[k] == '(') {
							write = true;
						}
					}
					break;
				}
				else {
					coeff.append(arr[j]);
				}
			}
			//System.out.println(new String(coeff));
			eqCoeffs.add(new BigDecimal(Double.parseDouble(new String(coeff))));
		}
		
		return eqCoeffs;
	}
	
//=================================================================================================
	
	/**
	 * Based on the private member attributes of the Grapher, this will create the graph with some
	 * name at some specified location.
	 * 
	 * @param name         Name of graph (title)
	 * @param destination  File which graph will be written
	 * 
	 * @throws IOException if destination file is not writable
	 */
	public void printGraph(String name, File destination) throws IOException {
		OutputGraph graph = new OutputGraph(destination.getCanonicalPath(), name, this);
		//graph.pack();
		//graph.setVisible(true);
		graph.output(destination);
		System.out.println("Generated Graph   : " + destination.getCanonicalPath());
	}
//=================================================================================================	
	
//=================================================================================================	
	
	/**
	 * An OutputGraph utilizes the JFreeChart framework to create graphs. These graphs will hold
	 * three data components:
	 * 		1. All Least-Squares Equations (Global)
	 * 		2. All Interpolation Equations (Local)
	 * 		3. All Data Points from data set
	 * 
	 * @author  Alexander Wojtowicz
	 * @version 1.0
	 * @since   2019-04-28
	 */
	public class OutputGraph extends JFrame {
		
		private static final long serialVersionUID = 1L;
		private JFreeChart chart;
		private Grapher grapher;
//=================================================================================================
		
		/**
		 * Creates and OutputGraph with a given title (for both graph and application window) and
		 * a particular Grapher.
		 * 
		 * @param applicationTitle  Title for application window
		 * @param chartTitle        Title for the chart (graph)
		 * @param grapher           Collection of data needed for graphing
		 */
	    public OutputGraph(String applicationTitle, String chartTitle, Grapher grapher) {
	        super(applicationTitle);
	        this.grapher = grapher;
	        XYSeriesCollection dataset = createDataset();
	        this.chart = createChart(dataset, chartTitle);
	        ChartPanel chartPanel = new ChartPanel(this.chart);
	        chartPanel.setPreferredSize(new Dimension(this.determineWidth(), this.determineHeight()));
	        setContentPane(chartPanel);    
	    }
//=================================================================================================
	    
	    /**
	     * Saves the OutputGraph to a specified file.
	     * 
	     * @param destination file
	     */
	    public void output(File destination) throws IOException {
	    	ChartUtilities.saveChartAsPNG(destination, 
	    			this.chart, 
	    			this.determineWidth(), 
	    			this.determineHeight());
	    }
//=================================================================================================

	    /**
	     * Creates three data sets:
	     * 		1. Data relating to Least-Squares
	     * 		2. Data relating to Interpolation
	     * 		3. Data points (scatter)
	     * 
	     * @return dataset  These three data sets all contained in one collection
	     */
	    public XYSeriesCollection createDataset() {
	    	XYSeries leastSquaresSeries = new XYSeries("Least-Squares");
	    	XYSeries interpolationSeries = new XYSeries("Interpolation");
	    	XYSeries pointsSeries = new XYSeries("Data Values");
	    	
	        for(Vector<BigDecimal> coeffs : this.grapher.getLeastSquaresEqs().keySet()) {
	        	BigDecimal[] coeffArr = new BigDecimal[coeffs.size()];
	        	for(int i=0; i<coeffs.size(); i++) {
	        		coeffArr[i] = coeffs.get(i);
	        	}
	        	Vector<Point2D.Double> points = Grapher.findCoordinatesForPolynomial(
	            		this.grapher.getLeastSquaresEqs().get(coeffs)[0], 
	            		this.grapher.getLeastSquaresEqs().get(coeffs)[1], 
	            		coeffArr,
	            		this.findPrecision(
	            				this.grapher.getLeastSquaresEqs().get(coeffs)[0], 
	            				this.grapher.getLeastSquaresEqs().get(coeffs)[1])
	            		);
	        	for(int i=0; i<points.size(); i++) {
	        		leastSquaresSeries.add(points.get(i).x, points.get(i).y);
	        	}
	        }
	        
	        for(Vector<BigDecimal> coeffs : this.grapher.getInterpolationEqs().keySet()) {
	        	BigDecimal[] coeffArr = new BigDecimal[coeffs.size()];
	        	for(int i=0; i<coeffs.size(); i++) {
	        		coeffArr[i] = coeffs.get(i);
	        	}
	        	Vector<Point2D.Double> points = Grapher.findCoordinatesForPolynomial(
	            		this.grapher.getInterpolationEqs().get(coeffs)[0], 
	            		this.grapher.getInterpolationEqs().get(coeffs)[1], 
	            		coeffArr,
	            		this.findPrecision(
	            				this.grapher.getInterpolationEqs().get(coeffs)[0],
	            				this.grapher.getInterpolationEqs().get(coeffs)[1])
	            		);
	        	for(int i=0; i<points.size(); i++) {
	        		interpolationSeries.add(points.get(i).x, points.get(i).y);
	        	}
	        }
	        
	        for(int i=0; i<this.grapher.getPoints().size(); i++) {
	        	pointsSeries.add(this.grapher.getPoints().get(i).x, this.grapher.getPoints().get(i).y);
	        }
	        
	        XYSeriesCollection dataset = new XYSeriesCollection();
	        dataset.addSeries(leastSquaresSeries);
	        dataset.addSeries(interpolationSeries);
	        dataset.addSeries(pointsSeries);
	        
	        return dataset;
	    }
//=================================================================================================
	    
	    /**
	     * Creates the chart (graph) with the three data sets.
	     * 
	     * @param dataset  The collection of the three data sets
	     * @param title    Title of the graph
	     * 
	     * @return A new chart with the three data sets (one collection) formatted for window display
	     */
	    public JFreeChart createChart(XYSeriesCollection dataset, String title) {
	        JFreeChart chart = ChartFactory.createXYLineChart(title, "X-AXIS", "Y-AXIS", dataset);
	        XYPlot plot = (XYPlot)chart.getPlot();
	        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
	        renderer.setSeriesPaint(0, Color.RED);
	        renderer.setSeriesLinesVisible(0, true);
	        renderer.setSeriesShapesVisible(0, false);
	        renderer.setSeriesPaint(1, Color.BLUE);  
	        renderer.setSeriesLinesVisible(1, true);
	        renderer.setSeriesShapesVisible(1, false);
	        renderer.setSeriesShape(2, ShapeUtilities.createDiamond(new Float(2)));
	        renderer.setSeriesPaint(2, Color.BLACK);  
	        renderer.setSeriesLinesVisible(2, false);
	        renderer.setSeriesShapesVisible(2, true);
	       
	        plot.setRenderer(renderer);
	        
	        return chart;
	    }    
//=================================================================================================
		
	    /**
		 * Determines idea width for the graph.
		 * 
		 * @return graph width
		 */
		public int determineWidth() {
			final int pixelsBetweenPoints = 20;
			int width = pixelsBetweenPoints * this.grapher.getPoints().size();
			if(width < 500) {
				width = 500;
			}
			
			return width;
		}
//=================================================================================================
		
		/** 
		 * Determines the ideal height for the graph.
		 * 
		 * @return graph height
		 */
		public int determineHeight() {
			return 500;
		}
//=================================================================================================
		
		/**
		 * Determines the precision based on domain.
		 * 
		 * @param xStart
		 * @param xEnd
		 * 
		 * @return precision
		 */
		public Double findPrecision(Double xStart, Double xEnd) {
			final int pointsBetween = 200;
			Double range = xEnd - xStart;
			
			return new Double(range / pointsBetween);
		}
//=================================================================================================
		
	} // End OutputGraph Class
//=================================================================================================	
	
}
