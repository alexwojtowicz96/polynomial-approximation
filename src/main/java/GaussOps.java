/**
 * A container for Gauss-Jordan elimination functions. These methods are to be used with 
 * augmented matrices.
 * 
 * @author  Alexander Wojtowicz
 * @version 1.0
 * @since   2019-04-11
 */

import java.util.Vector;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class GaussOps {

	public GaussOps() {}
//=================================================================================================
	
	/**
	 * Given an AugmentedMatrix and the row index of the matrix, scale it by some factor.
	 * 
	 * @param matrix
	 * @param index of row
	 * @param scalingFactor
	 * 
	 * @return scaled matrix
	 */
	public static AugmentedMatrix scale(AugmentedMatrix matrix, int index, BigDecimal scalingFactor) {
		if(index < 0 || index >= matrix.size()) {
			System.out.println("Warning:  Could not scale row '" + index + "'. Out of bounds!");
		}
		else {
			int size = matrix.get(index).size();
			for(int i=0; i<size; i++) {
				BigDecimal newVal = matrix.get(index).get(i).multiply(scalingFactor);
				matrix.get(index).remove(i);
				matrix.get(index).add(i, newVal);
			}
		}
		
		return matrix;
	}
//=================================================================================================
	
	/**
	 * Given two rows, row1 and row2, subtract row2 from row1 and make that row1 in the AugmentedMatrix.
	 * row1 -> row1 - row2
	 * 
	 * @param matrix
	 * @param index of row1 (What you are subtracting from)
	 * @param index of row2 (What you want to subtract)
	 * 
	 * @return matrix a new row of values
	 */
	public static AugmentedMatrix subtract(AugmentedMatrix matrix, int row1, int row2) {
		if(row1 == row2) {
			System.out.println("Warning:  Could not subtract row '" + row1 + "' from itself." );
		}
		else {
			int size = matrix.get(row1).size();
			for(int i=0; i<size; i++) {
				BigDecimal newVal = matrix.get(row1).get(i);
				if(!matrix.get(row2).get(i).equals(new BigDecimal(0))) {
					newVal = newVal.subtract(matrix.get(row2).get(i));
				}
				matrix.get(row1).remove(i);
				matrix.get(row1).add(i, newVal);
			}
		}
		
		return matrix;
	}
//=================================================================================================
	
	/**
	 * Given two rows, row1 and row2, swap their row indexes in the AugmentedMatrix.
	 * 
	 * @param matrix
	 * @param index of row1
	 * @param index of row2
	 * 
	 * @return matrix with swapped rows
	 */
	public static AugmentedMatrix swap(AugmentedMatrix matrix, int index1, int index2) {
		if((index1 < 0 || index1 >= matrix.size()) || (index2 < 0 || index2 >= matrix.size())) {
			if(index1 != -99 /* This means maxIndex in solve() was never set... so we don't swap */) {
				System.out.println("Warning:  Could not swap row '" + index1 + "' with row '" + index2 +
								   "'. Out of bounds!");
			}
		}
		else if(index1 != index2) {
			Vector<BigDecimal> row1 = new Vector<BigDecimal>(matrix.get(index1));
			Vector<BigDecimal> row2 = new Vector<BigDecimal>(matrix.get(index2));
			matrix.remove(index1);
			matrix.add(index1, row2);
			matrix.remove(index2);
			matrix.add(index2, row1);
		}
		// else:  Do nothing
		
		return matrix;
	}
//=================================================================================================
	
	/**
	 * Given the index of a column in the matrix, and a starting position index,
	 * find the row with the largest value in that column and return its index.
	 * 
	 * @param matrix
	 * @param index of some column
	 * @param starting row in the column
	 * 
	 * @return index which holds the max value in the column from startPos to colSize-1
	 */
	public static int findLargestRowByColumn(AugmentedMatrix matrix, int colIndex, int startPos) {
		BigDecimal max = new BigDecimal(Double.MIN_VALUE);
		int maxIndex = -99;
		if(colIndex < 0 || colIndex >= matrix.get(0).size()) {
			System.out.println("Warning:  Could not find largest row by column. Index '" + colIndex + 
							   "' is out of bounds!");
		}
		else {
			Vector<Vector<BigDecimal>> tMatrix = MatrixOps.transpose(matrix.getMatrix());
			for(int i=startPos; i<tMatrix.get(colIndex).size(); i++) {
				if(tMatrix.get(colIndex).get(i).doubleValue() > max.doubleValue()) {
					maxIndex = i;
					max = tMatrix.get(colIndex).get(i);
				}
			}
		}
		
		return maxIndex;
	}
//=================================================================================================
	
	/**
	 * Performs row operations on the matrix until it has reached an upper triangular form.
	 * Precondition:  The matrix must be augmented, and the system must be consistent!
	 * To avoid inconsistency, the ideal matrix will be of size NxN+1.
	 * 
	 * @param matrix
	 * 
	 * @return solved matrix
	 */
	public static AugmentedMatrix solve(AugmentedMatrix matrix) {
		int precision = 512;
		int size = matrix.size();
		for(int i=0; i<size; i++) {
			int maxIndex = GaussOps.findLargestRowByColumn(matrix, i, i);
			matrix = GaussOps.swap(matrix, maxIndex, i);
			matrix = GaussOps.scale(matrix, i, new BigDecimal(1).divide(
					matrix.get(i).get(i), 
					precision, 
					RoundingMode.HALF_UP));
			matrix = GaussOps.eliminate(matrix, i, i);
			for(int j=i; j<size; j++) {
				if(j == i) {
					matrix.setValue(i, i, new BigDecimal(1));
				}
				else {
					matrix.setValue(j, i, new BigDecimal(0));
				}
			}
		}
		matrix = GaussOps.backSolve(matrix);
		
		return matrix;
	}
//=================================================================================================
	
	/**
	 * Given an index of a row and the index of a column, make everything under that row in that 
	 * column a zero by adding and subtracting row operations.
	 * 
	 * @param matrix
	 * @param rowIndex
	 * @param colIndex
	 * 
	 * @return matrix with eliminated columns
	 */
	public static AugmentedMatrix eliminate(AugmentedMatrix matrix, int rowIndex, int colIndex) {
		int precision = 512;
		int size = matrix.size();
		for(int i=rowIndex; i<size-1; i++) {
			if(!(matrix.get(i+1).get(colIndex).equals(new BigDecimal(0)))) {
				//System.out.println("not zero");
				matrix = GaussOps.scale(matrix, 
						i+1, 
						new BigDecimal(1).divide(matrix.get(i+1).get(colIndex), 
								precision, 
								RoundingMode.HALF_UP));
				matrix = GaussOps.subtract(matrix, i+1, rowIndex);
			}
			// else == 0; Do nothing
			else {
			}
		}
		
		return matrix;
	}
//=================================================================================================
	
	/**
	 * Substitutes each value into the matrix to create an identity matrix augmented with an Nx1.
	 * Start at the bottom row and work up.
	 * 
	 * @param upper-triangular matrix
	 * 
	 * @return solved matrix
	 */
	public static AugmentedMatrix backSolve(AugmentedMatrix matrix) {
		BigDecimal[] values = new BigDecimal[matrix.get(0).size()-1];
		int size = matrix.size();
		for(int i=size-1; i>-1; i--) {
			BigDecimal val = (matrix.get(i).get(matrix.get(i).size()-1));
			Vector<BigDecimal> rowCoeffs = new Vector<BigDecimal>();
			for(int j=i; j<size; j++) {
				rowCoeffs.add(matrix.get(i).get(j));
			}
			values[i] = val;
			if(rowCoeffs.size() != 1) {
				int idx = values.length-1;
				//System.out.println("values.length : " + idx);
				for(int j=1; j<rowCoeffs.size(); j++) {
					//System.out.println("    j=" + j);
					//System.out.println("idx : " + idx);
					if(!rowCoeffs.get(rowCoeffs.size()-j).equals(new BigDecimal(0))) {
						//System.out.println("not zero...");
						values[i] = values[i].subtract((rowCoeffs.get(rowCoeffs.size()-j)).multiply(values[idx]));
					}
					//System.out.println(values[i]);
					idx -= 1;
				}
			}
			//System.out.println(rowCoeffs + " === " + values[i]);
		}
		
		// Formatting the matrix (1's and 0's should be in certain known positions)
		int width = matrix.get(0).size();
		for(int i=0; i<size; i++) {
			for(int j=0; j<width; j++) {
				if(i == j) {
					matrix.setValue(i, j, new BigDecimal(1));
				}
				else {
					if(j != width-1) {
						matrix.setValue(i, j, new BigDecimal(0));
					}
					else {
						matrix.setValue(i, j, values[i]);
					}
				}
			}
		}
		
		return matrix;
	}
//=================================================================================================
	
}
