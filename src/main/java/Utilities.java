/**
 * A class of basic I/O and approximation utilities to get data and overall 
 * help to organize the code.
 * 
 * @author  Alexander Wojtowicz
 * @version 1.0
 * @since   2019-04-11
 */

import java.awt.geom.Point2D;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Scanner;
import java.util.Set;
import java.util.Vector;

public class Utilities {
	
	public Utilities() {}
//=================================================================================================
	
	/**
	 * Checks the format of a supplied file containing a matrix (not Augmented). 
	 * Matrices from file should be NxM, where all N rows have M entries separated by any number 
	 * of white spaces. Only real numbers are allowed.
	 * 
	 * @param file
	 * 
	 * @return true if valid, false if not valid
	 * 
	 * @throws FileNotFoundException if file does not exist
	 */
	public static boolean matrixFileIsValid(File file) throws FileNotFoundException {
		Scanner reader = new Scanner(file);
		boolean flag = true;
		
		try {
			Vector<String[]> lines = new Vector<String[]>();
			while(reader.hasNextLine()) {
				String line = reader.nextLine();
				lines.add(line.split("\\s+"));
			}
			reader.close();
			Set<Integer> rowSizes = new HashSet<Integer>();
			for(String[] row : lines) {
				rowSizes.add(row.length);
				for(String elem : row) {
					Float.parseFloat(elem);
				}
			}
			if(rowSizes.size() != 1) {
				flag = false;
			}
		}
		catch(Exception e) {
			flag = false;
		}
		
		return flag;
	}
//=================================================================================================

	/**
	 * Given some file, parse the data into a structure (LinkedHashMap<Integer, Vector<Double>>),
	 * where the Integer is the X value (time in most cases), and the Vector<Double> is a list
	 * of all the Y values for each column at that X value. The file should mimic the following:
	 * 
	 * +0.98units   +1.55units   -9.01units
	 * +5.11units   -3.54units   +8.11units
	 * -4.33units   +1.10units   +5.21units
	 * 
	 * The time interval should be known, but not in the file. This will be supplied as a parameter.
	 * The units may or may not be included in the file. Values can be any real number or any 
	 * integer and separated by any number of white spaces (horizontally, NOT vertically).
	 * Precondition:  Every row must have the same number of columns, or else this will not work!
	 * Precondition:  The time interval must remain constant throughout the file (It can't be 
	 * something like:  t=2, t=4, t=6, t=7, t=9, t=12... needs to be the same throughout).
	 * Precondition:  Time interval must be a positive integer.
	 * 
	 * @param String representation of the file (path of) to be parsed
	 * @param known time interval separating each row
	 * 
	 * @return The data in the form of a LinkdHashMap<Integer, Vector<Double>>
	 */
	public static LinkedHashMap<Integer, Vector<Double>> populateDataSet(String filePath, int timeInterval) {
		LinkedHashMap<Integer, Vector<Double>> fileData = new LinkedHashMap<Integer, Vector<Double>>();
		String msg = "Error:  Something went wrong while parsing '" + filePath + "'.";
		
		try {
			File file = new File(filePath);
			Scanner reader = new Scanner(file);
			int counter = 0;
			while(reader.hasNextLine()) {
				String row = reader.nextLine();
				String[] lineVals = row.split("\\s+");
				Vector<Double> unitlessRow = new Vector<Double>();
				for(int i=0; i<lineVals.length; i++) {
					unitlessRow.add(Utilities.trimUnits(lineVals[i]));
				}
				fileData.put((counter * timeInterval), unitlessRow);
				counter += 1;
			}
			reader.close();
			
			counter = 0;
			Set<Integer> numsInCols = new HashSet<Integer>();
			for(int i=0; i<timeInterval*fileData.size(); i+=timeInterval) {
				numsInCols.add(fileData.get(i).size());
			}
			if(numsInCols.size() != 1) {
				throw new Exception("Invalid format:  Found variable row lengths!");
			}
		}
		catch(Exception e) {
			System.out.println(msg);
			e.printStackTrace();
		}
		
		return fileData;
	}
//=================================================================================================
	
	/**
	 * Given a number with units attached to it (in String form), trim the units off, and
	 * transform the type into a floating point value.
	 * 
	 * @param value  A string with units
	 * 
	 * @return value  A double value with no units
	 */
	public static double trimUnits(String value) {
		char[] valArr = value.toCharArray();
		StringBuilder builder = new StringBuilder();
		for(int i=0; i<valArr.length; i++) {
			Character currChar = new Character(valArr[i]);
			if(Character.isDigit(currChar) || 
					currChar.equals('.') || 
					currChar.equals('-') || 
					currChar.equals('+')) 
			{
				builder.append(currChar); 
			}
			else {
				break;
			}
		}
		
		return Double.parseDouble(new String(builder));
	}
//=================================================================================================

	/**
	 * Given a file, return its file name with no .extension or path.
	 * 
	 * @param A valid file
	 * 
	 * @throws IOException if path of file is missing
	 */
	public static String getRawFileName(File file) throws IOException {
		StringBuilder builder = new StringBuilder();
		char[] filePath = file.getCanonicalFile().toString().toCharArray();
		for(int i=filePath.length-1; i>-1; i--) {
			String ch = new String();
			ch += filePath[i];
			if(ch.equals(System.getProperty("file.separator"))) {
				break;
			}
			else {
				builder.append(filePath[i]);
			}
		}
		builder = builder.reverse();
		char[] fname = new String(builder).toCharArray();
		int dotIdx = 1;
		for(int i=fname.length-1; i>-1; i--) {
			if(fname[i] == '.') {
				dotIdx = i;
				break;
			}
		}
		builder = new StringBuilder();
		for(int i=0; i<dotIdx; i++) {
			builder.append(fname[i]);
		}
		
		return new String(builder);
	}
//=================================================================================================
	
	/**
	 * Returns a string representation of a dataSet. This will have the first column be the x values,
	 * where every other column is a corresponding y value.
	 * 
	 * @param dataSet  LinkedHashMap<Integer, Vector<Double>>
	 * 
	 * @return string representation of the dataSet (basically what was read in, only now
	 * 		   there are x values in the left-most column
	 */
	public static String dataSetToString(LinkedHashMap<Integer, Vector<Double>> dataSet) {
		Vector<Vector<String>> dataStrings = new Vector<Vector<String>>();
		int numOfCols = dataSet.get(0).size();
		Vector<String> headings = new Vector<String>();
		headings.add("X");
		for(int i=0; i<numOfCols; i++) {
			headings.add("Y_" + i);
		}
		dataStrings.add(headings);
		for(int x : dataSet.keySet()) {
			Vector<String> row = new Vector<String>();
			row.add(new Integer(x).toString());
			for(int i=0; i<dataSet.get(x).size(); i++) {
				row.add(new Double(dataSet.get(x).get(i)).toString());
			}
			dataStrings.add(row);
		}
		int[] maxWidths = new int[dataStrings.get(0).size()];
		for(int i=0; i<dataStrings.get(0).size(); i++) {
			int max = -1;
			for(int j=0; j<dataStrings.size(); j++) {
				if(dataStrings.get(j).get(i).length() > max) {
					max = dataStrings.get(j).get(i).length();
				}
			}
			maxWidths[i] = max;
		}
		
		StringBuilder builder = new StringBuilder();
		for(int i=0; i<dataStrings.size(); i++) {
			for(int j=0; j<dataStrings.get(i).size(); j++) {
				int numSpaces = maxWidths[j] - dataStrings.get(i).get(j).length();
				builder.append(dataStrings.get(i).get(j));
				for(int k=0; k<numSpaces+2; k++) {
					builder.append(" ");
				}
				if(j == 0) {
					builder.append("|  ");
				}
			}
			builder.append("\n");
			if(i == 0) {
				for(int j=0; j<dataStrings.get(i).size(); j++) {
					for(int k=0; k<maxWidths[j]; k++) {
						builder.append("-");
					}
					builder.append("  ");
					if(j == 0) {
						builder.append("|  ");
					}
				}
				builder.append("\n");
			}
		}
			
		return new String(builder);
	}
//=================================================================================================
	
	/**
	 * Prints usage error message to standard output.
	 */
	public static void printUsageError() {
		System.out.println("Usage Error:  Supply 4 parameters at the command line.");
		System.out.println("Example:      java -jar Approximate-1.0.jar path/to/file.txt 30 1 linear");
	}
//=================================================================================================

	/**
	 * Given the domain for x values, an approximation equation, and a type of approximation 
	 * equation, create a string that conveniently displays this information for output.
	 * 
	 * @param xStart  x_k -> number where x domain begins (inclusive)
	 * @param xEnd    x_k+1 -> number where x domain ends (exclusive)
	 * @param eq      String version of the approximation equation
	 * @param type    Either 'least-squares' or 'interpolation'
	 * 
	 * @return a string with all this information formatted for output
	 */
	public static String formatEqOutput(int xStart, int xEnd, String eq, String type) {
		StringBuilder builder = new StringBuilder();
		builder.append(xStart);
		builder.append(" <= x < ");
		builder.append(xEnd);
		builder.append("; ");
		builder.append(eq);
		builder.append("; ");
		builder.append(type);
		
		return new String(builder);
	}
//=================================================================================================

	/**
	 * Converts a string representation of a very small double value of the form 1.000E-6 to 
	 * 1.000*10^(-6)
	 * 
	 * @param original  The original string containing 'E'
	 * 
	 * @return the new string without 'E' in a new format
	 */
	public static String convertEto10(String original) {
		StringBuilder builder = new StringBuilder();
		char[] valArr = original.toCharArray();
		String coeff = new String();
		String exp = new String();
		int Eidx = 0;
		for(int k=0; k<valArr.length; k++) {
			if(valArr[k] == 'E') {
				Eidx = k;
				break;
			}
			else {
				coeff += valArr[k];
			}
		}
		for(int k=Eidx+1; k<valArr.length; k++) {
			exp += valArr[k];
		}
		builder.append(coeff + "*10^(" + exp + ")");
		
		return new String(builder);
	}
//=================================================================================================
	
	/**
	 * Formats and prints the equation data to a specified file.
	 * 
	 * @param eqData   A list of strings where each string is a row representing an equation for some
	 * 				   function with respect to 'x'
	 * @param fileName Name of output file 
	 * 
	 * @throws FileNotFoundException if specified file is not found
	 */
	public static void printDataToFile(Vector<String> eqData, String fileName) throws FileNotFoundException {	
		Vector<String[]> rows = new Vector<String[]>();
		final String TERMINATOR = ";";
		
		for(int i=0; i<eqData.size(); i++) {
			String[] row = eqData.get(i).split(";\\s");
			rows.add(row);
		}
		Integer[] maxColWidths = new Integer[rows.get(0).length];
		for(int i=0; i<rows.get(0).length; i++) {
			int max = 0;
			for(int j=0; j<rows.size(); j++) {
				if(max < rows.get(j)[i].length()) {
					max = rows.get(j)[i].length();
				}
			}
			maxColWidths[i] = max;
		}
		
		PrintWriter writer = new PrintWriter(new File(fileName)); 
		for(int i=0; i<rows.size(); i++) {
			for(int j=0; j<rows.get(i).length; j++) {
				writer.print(rows.get(i)[j]);
				if(j != rows.get(i).length-1) {
					writer.print(TERMINATOR + " ");
					int numSpaces = maxColWidths[j] - rows.get(i)[j].length();
					final int BUFFER = 2;
					for(int k=0; k<numSpaces+BUFFER; k++) {
						writer.print(" ");
					}
				}
				else {
					writer.print("\n");
				}
			}
		}
		writer.close();
		System.out.println("Wrote To File     : " + fileName);
	}
//=================================================================================================
	
	/**
	 * Given a data set and a column index, return a vector of (x, y) points where x is the 
	 * key of the data set and y is the index of the vector at that key value.
	 * 
	 * @param dataSet  A valid data set of x values mapped to y values in N rows
	 * @param idx      The index of the y-column
	 * 
	 * @return points  A vector of all points which should be automatically sorted in 
	 *    			   ascending x order
	 */
	public static Vector<Point2D.Double> getPointsAtIndex(
			LinkedHashMap<Integer, Vector<Double>> dataSet, int idx) {
		Vector<Point2D.Double> points = new Vector<Point2D.Double>();
		
		for(Integer key : dataSet.keySet()) {
			Double x = new Double(key);
			Double y = dataSet.get(key).get(idx);
			points.add(new Point2D.Double(x, y));
		}
		
		return points;
	}
//=================================================================================================
	
	/**
	 * Creates a string to represent the time elapsed. This is only going to work if the days do
	 * not overlap... So if it takes longer than 24 hours, or you do something like begin at 
	 * 11:59pm and end at 12:01am the next day, it won't work.
	 * 
	 * @param begin time
	 * @param end time
	 * 
	 * @return formatted string representation of (end time - begin time)
	 */
	public static String timeElapsed(String startTime, String endTime) {
		StringBuilder timeString = new StringBuilder();
		String[] start = startTime.split(":");
		String[] end = endTime.split(":");
		int sSecs = (Integer.parseInt(start[0])*60*60) + 
				(Integer.parseInt(start[1])*60) + 
				(Integer.parseInt(start[2]));
		int eSecs = (Integer.parseInt(end[0])*60*60) + 
				(Integer.parseInt(end[1])*60) + 
				(Integer.parseInt(end[2]));
		int timeElapsed = eSecs - sSecs;
		if(timeElapsed < 0) {
			timeString.append("ERROR");
		}
		else {
			int[] time = {0, 0, timeElapsed};
			while(time[2] >= 60) {
				time[1] += 1;
				time[2] -= 60;
			}
			while(time[1] >= 60) {
				time[0] += 1;
				time[1] -= 60;
			}
			for(int i=0; i<time.length; i++) {
				if(new Integer(time[i]).toString().length() == 1) {
					timeString.append("[0" + new Integer(time[i]).toString() + "]");
				}
				else {
					timeString.append("[" + new Integer(time[i]).toString() + "]");
				}
				if(i == 0) {
					timeString.append("HOURS ");
				}
				if(i == 1) {
					timeString.append("MIN ");
				}
				if(i == 2) {
					timeString.append("SEC");
				}
			}
		}
		
		return new String(timeString);
	}
//=================================================================================================
	
}
