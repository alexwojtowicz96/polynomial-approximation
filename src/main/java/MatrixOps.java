/**
 * A container of basic matrix arithmetic operations.
 * 
 * @author  Alexander Wojtowicz
 * @version 1.0
 * @since   2019-04-11
 */

import java.math.BigDecimal;
import java.util.Vector;

public class MatrixOps {

	public MatrixOps() {}
//=================================================================================================
		
	/**
	 * Given matrix1 and matrix2, subtract matrix2 from matrix1. If they are not
	 * compatible, return empty Matrix.
	 * Precondition:  matrix1 and matrix2 are valid Matrix objects.
	 * 
	 * @param matrix1
	 * @param matrix2
	 * 
	 * @return result = matrix1 - matrix2
	 */
	public static Matrix subtract(Matrix matrix1, Matrix matrix2) {
		Vector<Vector<BigDecimal>> result = new Vector<Vector<BigDecimal>>();
		if(matrix1.size() == matrix2.size() && matrix1.getRow(0).size() == matrix2.getRow(0).size()) {
			for(int i=0; i<matrix1.size(); i++) {
				Vector<BigDecimal> row = new Vector<BigDecimal>();
				for(int j=0; j<matrix1.getRow(i).size(); j++) {
					row.add((matrix1.get(i, j)).subtract((matrix2.get(i, j))));
				}
				result.add(row);
			}
		}
		else {
			System.out.println("Error:  Could not subtract matrices. Incompatible dimensions!");
		}
			
		return new Matrix(result);
	}
//=================================================================================================
	
	/**
	 * Given matrix1 and matrix2, add matrix2 to matrix1. If they are not
	 * compatible, return empty Matrix.
	 * Precondition:  matrix1 and matrix2 are valid Matrix objects.
	 * 
	 * @param matrix1
	 * @param matrix2
	 * 
	 * @return result = matrix1 + matrix2
	 */
	public static Matrix add(Matrix matrix1, Matrix matrix2) {
		Vector<Vector<BigDecimal>> result = new Vector<Vector<BigDecimal>>();
		if(matrix1.size() == matrix2.size() && matrix1.getRow(0).size() == matrix2.getRow(0).size()) {
			for(int i=0; i<matrix1.size(); i++) {
				Vector<BigDecimal> row = new Vector<BigDecimal>();
				for(int j=0; j<matrix1.getRow(i).size(); j++) {
					row.add((matrix1.get(i, j)).add((matrix2.get(i, j))));
				}
				result.add(row);
			}
		}
		else {
			System.out.println("Error:  Could not add matrices. Incompatible dimensions!");
		}
			
		return new Matrix(result);
	}
//=================================================================================================
	
	/**
	 * Finds the dot product of matrix1 with matrix2. If they are not compatible, return empty Matrix.
	 * Precondition:  matrix1 and matrix2 are valid Matrix objects.
	 * 
	 * @param matrix1
	 * @param matrix2
	 * 
	 * @return result = matrix1 . matrix2
	 */
	public static Matrix dotProduct(Matrix matrix1, Matrix matrix2) {
		Vector<Vector<BigDecimal>> result = new Vector<Vector<BigDecimal>>();
		if(matrix1.getRow(0).size() == matrix2.size()) {
			for(int i=0; i<matrix1.size(); i++) {
				Vector<BigDecimal> row = new Vector<BigDecimal>();
				for(int j=0; j<matrix2.getRow(0).size(); j++) {
					BigDecimal val = new BigDecimal(0);
					for(int k=0; k<matrix1.getRow(0).size(); k++) {
						val = val.add(matrix1.getRow(i).get(k).multiply(matrix2.getCol(j).get(k)));
					}
					row.add(val);
				}
				result.add(row);
			}
		}
		else {
			System.out.println("Error:  Could not dot product matrices. Incompatible dimensions!");
		}
		
		return new Matrix(result);
	}
//=================================================================================================
	
	/**
	 * Given a matrix, transpose it, then return the result.
	 * The result is not a Matrix object! It is the same structure as Matrix::matrix however.
	 * 
	 * @param matrix-like structure (Vector<Vector<BigDecimal>>)
	 * 
	 * @return matrix transposed (Vector<Vector<BigDecimal>>)
	 */
	public static Vector<Vector<BigDecimal>> transpose(Vector<Vector<BigDecimal>> matrix) {
		Vector<Vector<BigDecimal>> result = new Vector<Vector<BigDecimal>>();
		for(int z=0; z<matrix.get(0).size(); z++) {
			Vector<BigDecimal> transRow = new Vector<BigDecimal>();
			for(int i=0; i<matrix.size(); i++) {
				for(int j=0; j<matrix.get(i).size(); j++) {
					if(j == z) {
						transRow.add(matrix.get(i).get(j));
					}
				}
			}
			result.add(transRow);
		}
		
		return result;
	}
//=================================================================================================	
	
}
