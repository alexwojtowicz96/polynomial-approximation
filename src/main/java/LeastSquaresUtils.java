/**
 * A collection of functions dealing with least-squares approximation. Least-squares approximation
 * is used to approximate a global function of best fit given a particular data set.
 * 
 * @author  Alexander Wojtowicz
 * @version 1.0
 * @since   2019-04-11
 */

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.Vector;

public class LeastSquaresUtils {
	
	public LeastSquaresUtils() {}	
//=================================================================================================

	/**
	 * Given a data set of type LinkedHashMap<Integer, Vector<Double>>, generate the X matrix for a 
	 * approximation with any polynomial of the form:  f(x) = C0 + C1X + C2X^2 + ... + CNX^N.
	 * The approximation function is generalized to include all of the CiX^i terms. 
	 * This will always be an NxL matrix since we have L basis functions X0, X1, ..., XL-1.
	 * Precondition:  dataSet must be of valid format.
	 * Precondition:  degree must be non-negative.
	 * 
	 * @param dataSet  (generated from Utilities::populateDataSet())
	 * @param degree   (highest degree of the polynomial)
	 * 
	 * @return The X matrix where Xi = X^i
	 */
	public static Matrix getXMatrixForPolynomial(LinkedHashMap<Integer, Vector<Double>> dataSet, int degree) {
		Vector<Vector<BigDecimal>> matrix = new Vector<Vector<BigDecimal>>();
		
		Set<Integer> xValues = dataSet.keySet();
		for(int x : xValues) {
			Vector<BigDecimal> row = new Vector<BigDecimal>();
			row.add(new BigDecimal(1));
			double val = x;
			for(int i=0; i<degree; i++) {
				row.add(new BigDecimal(val));
				val *= x;
			}
			matrix.add(row);
		}
		
		return new Matrix(matrix);
	}
//=================================================================================================
	
	/**
	 * Given an index for the column in the Vector of Y values, generate an Nx1 matrix to represent
	 * the Y matrix for approximation. This should work for any approximation function.
	 * This will always be an Nx1 matrix since Y is the value at some X.
	 * Precondition:  The index referring the the column must be in bounds.
	 * 
	 * @param dataSet  (generated from Utilities::populateDataSet())
	 * @param column   (The index for a column within all of the rows in the dataSet)
	 * 
	 * @return The Y matrix (Nx1)
	 */
	public static Matrix getYMatrix(LinkedHashMap<Integer, Vector<Double>> dataSet, int column) {
		Vector<Vector<BigDecimal>> matrix = new Vector<Vector<BigDecimal>>();
		
		Set<Integer> xValues = dataSet.keySet();
		for(int x : xValues) {
			Vector<BigDecimal> row = new Vector<BigDecimal>();
			row.add(new BigDecimal(dataSet.get(x).get(column)));
			matrix.add(row);
		}
		
		return new Matrix(matrix);
		
	}
//=================================================================================================	
	
	/**
	 * Converts a matrix to an equation represented by a string.
	 * 
	 * @param matrix  A solved AugmentedMatrix, A|b, where A is NxN identity matrix and b is Nx1
	 * @param num     The subscript attached to the LHS of the equation, i.e., y_0 = ..., num = 0
	 * 
	 * @return string representation of the polynomial equation
	 */
	public static String approxEq(AugmentedMatrix matrix, Integer num) {
		StringBuilder builder = new StringBuilder();
		builder.append("y_");
		builder.append(num.toString());
		builder.append(" = ");
		for(int i=0; i<matrix.size(); i++) {
			String xValue = new String();
			if(new Double(matrix.get(i).get(matrix.get(i).size()-1).doubleValue()).toString().contains("E")) {
				String val = new Double(matrix.get(i).get(matrix.get(i).size()-1).doubleValue()).toString();
				builder.append(Utilities.convertEto10(val));
			}
			else {
				builder.append(new Double(matrix.get(i).get(matrix.get(i).size()-1).doubleValue()).toString());
			}
			
			if(i != 1 && i != 0) {
				xValue = "x^(" + i + ")";
			}
			else {
				if(i == 1) {
					xValue = "x";
				}
			}
			builder.append(xValue);
			if(i != matrix.size()-1) {
				builder.append(" + ");
			}
		}
		
		return new String(builder);
	}
//=================================================================================================	
	
}
