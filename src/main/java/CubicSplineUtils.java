/**
 * A collection of functions dealing with cubic spline interpolation.
 * 
 * Given two points, we want to find a cubic function that passes through both, but also has
 * smooth connections between all of the points. To do this, the following function can be used:
 * 
 * 		s(x) = [(x_j -x)^3(M_j-1) + (x - x_j-1)^3(M_j)] / [6(x_j - x_j-1)]
 *			 + [(x_j - x)(y_j-1) + (x - x_j-1)(y_i)] / [x_j - x_j-1]
 *			 - [(1/6)(x_j - x_j-1)] * [(x_j - x)(M_j-1) + (x - x_j-1)(M_j)]
 *
 * The M's in the above equation can be found by using the following method (system of equations):
 * 
 * 		{	M_1 = M_n = 0
 * 
 * 		{	(1/6) * (x_j - x_j-1)(M_j-1) 
 * 			+ (1/3) * (x_j+1 - x_j-1)(M_j) 
 * 			+ (1/6) * (x_j+1 - x_j)(M_j+1)
 * 			= [(y_j+1 - y_j) / (x_j+1 - x_j)] - [(y_j - y_j-1) / (x_j - x_j-1)]
 * 			; where j = 2, 3, 4, ..., n - 1 {n = number of points}
 * 
 * Note: In this notation, we start counting at 1 rather than 0, so M_1 is the first M constant,
 * x_1 is the first point's x value, and so on...
 * 
 * ************************************************************************************************
 * SOURCE:  http://mathonline.wikidot.com/natural-cubic-spline-function-interpolation-examples-1
 * ************************************************************************************************
 * 
 * Notice how s(x) may be a third degree polynomial, but it is not in standard form, based on this
 * information, we can actually rewrite it in standard form using algebraic manipulation.
 * If we do this, each of the constants a, b, c, and d in the following equation can be found:
 * 
 * 		s(x) = ax^3 + bx^2 + cx + d
 * ------------------------------------------------------------------------------------------------
 * 
 * 		a = [M_j - M_j-1] / [6(x_j - x_j-1)]
 * 	
 * 		b = [(M_j-1 * x_j) - (M_j * x_j-1)] / [2(x_j - x_j-1)]
 * 
 * 		c = [-3((M_j-1 * (x_j)^2) - (M_j * (x_j-1)^2)) 
 * 		  - 6(y_j-1 - y_j) + ((x_j - x_j-1)^2 * (M_j-1 - M_j))] / [6(x_j - x_j-1)]
 * 
 * 		d = [((M_j-1 * (x_j)^3) - (M_j * (x_j-1)^3)) + 6(y_j-1 * x_j) - y_j(x_j-1)
 * 		  - ((x_j - x_j-1)^2 * ((M_j-1 * x_j) - (M_j * x_j-1))))] / [6(x_j - x_j-1)]
 * 
 * @author  Alexander Wojtowicz
 * @version 1.0
 * @since   2019-04-17
 */

import java.awt.geom.Point2D;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Vector;

public class CubicSplineUtils {

	final static BigDecimal ONE_SIXTH = new BigDecimal(new Double(1)/new Double(6));
	final static BigDecimal ONE_THIRD = new BigDecimal(new Double(1)/new Double(3));
//=================================================================================================
	
	public CubicSplineUtils() {}
//=================================================================================================	
	
	/**
	 * Find all the M in M_0, M_1, M_2, ..., M_n-1. These coefficients relate to the slopes at the
	 * end points, and they help generate the smooth curve over the entire domain of points.
	 * 
	 * @param dataPoints  From a data set, this is each x point corresponding with each y point
	 * 		  		  	  in a predetermined column. These points should be in ascending x order
	 * 
	 * @return an AugmentedMatrix A|b where A is NxN and b is Nx1. Each row corresponds to M_i
	 * 		   where i = 0, 1, 2, 3, ..., N-1
	 */
	public static AugmentedMatrix genMMatrix(Vector<Point2D.Double> dataPoints) {
		int precision = 512;
		Vector<Vector<BigDecimal>> aVecs = new Vector<Vector<BigDecimal>>();
		Vector<Vector<BigDecimal>> bVecs = new Vector<Vector<BigDecimal>>();
		
		for(int i=1; i<dataPoints.size()-1; i++) {
			Vector<BigDecimal> row = new Vector<BigDecimal>();
			for(int k=1; k<i; k++) {
				row.add(new BigDecimal(0));
			}
			row.add(ONE_SIXTH.multiply(new BigDecimal(dataPoints.get(i).x - dataPoints.get(i-1).x)));
			row.add(ONE_THIRD.multiply(new BigDecimal(dataPoints.get(i+1).x - dataPoints.get(i-1).x)));
			row.add(ONE_SIXTH.multiply(new BigDecimal(dataPoints.get(i+1).x - dataPoints.get(i).x)));
			for(int k=0; k<(dataPoints.size()-(i-1)-3); k++) {
				row.add(new BigDecimal(0));
			}
			aVecs.add(row);
			
			row = new Vector<BigDecimal>();
			BigDecimal exp1 = (new BigDecimal(dataPoints.get(i+1).y - dataPoints.get(i).y).divide( 
					new BigDecimal(dataPoints.get(i+1).x - dataPoints.get(i).x), precision, RoundingMode.HALF_UP));
			BigDecimal exp2 = (new BigDecimal(dataPoints.get(i).y - dataPoints.get(i-1).y).divide(
					new BigDecimal(dataPoints.get(i).x - dataPoints.get(i-1).x), precision, RoundingMode.HALF_UP));
			BigDecimal ans = exp1.subtract(exp2);
			//System.out.println(exp1 + " - " + exp2 + " = " + ans);
			row.add(ans);
			bVecs.add(row);
		}
		Vector<BigDecimal> row = new Vector<BigDecimal>();
		row.add(new BigDecimal(1));
		for(int i=0; i<dataPoints.size()-1; i++) {
			row.add(new BigDecimal(0));
		}
		aVecs.add(row);
		row = new Vector<BigDecimal>();
		for(int i=0; i<dataPoints.size()-1; i++) {
			row.add(new BigDecimal(0));
		}
		row.add(new BigDecimal(1));
		aVecs.add(row);
		
		row = new Vector<BigDecimal>();
		row.add(new BigDecimal(0));
		bVecs.add(row);
		bVecs.add(row);

		return new AugmentedMatrix(new Matrix(aVecs), new Matrix(bVecs));
	}
//=================================================================================================
	
	/**
	 * Given a set of data points sorted in ascending x order, determine the coefficients x_0, x_1,
	 * x_2, and x_3 in the
	 * equation: 
	 * 
	 * 		s(x) = x_0(x^0) + x_1(x^1) + x_2(x^2) + x_3(x^3)
	 * 
	 * That will serve as a cubic approximation function for the data between points x_k-1 and x_k, 
	 * where; 1 <= k <= n (n is the number of points).
	 * 
	 * There will be N-1 sets of coefficients where N is the number of points. These 'sets' of points
	 * are stored in an array of <BigDecimal> values. All of these arrays are then stored in a 
	 * vector. For example, between points x_0 and x_1, vector.get(0)[i] represents each coefficient
	 * for the cubic function drawn between x_0 and x_1 where i = 0, 1, 2, 3.
	 * 
	 * @param dataPoints  From the data set, there are several columns; the x-values and one of the
	 * 		  			  y columns make up a Vector<Point2D.Float> called 'dataPoints'
	 * @param matrix      An M matrix found by calling CubicSplineUtils.genMMatrix(dataPoints) 
	 * 					  [NOT solved!]
	 * 
	 * @return A vector of all arrays of coefficients which make up each coefficient cubic polynomial 
	 * spline function in reverse standard form.
	 */
	public static Vector<BigDecimal[]> findCoeffs(Vector<Point2D.Double> dataPoints, AugmentedMatrix matrix) {
		int precision = 512;
		//matrix = GaussOps.solveMMatrix(matrix);
		matrix = GaussOps.solve(matrix);
		Vector<BigDecimal> mVals = matrix.getBValues();
		Vector<BigDecimal[]> constantsVec = new Vector<BigDecimal[]>();
		
		for(int i=1; i<mVals.size(); i++) {
			// constants[0] = x0, constants[1] = x1, constants[2] = x2, constants[3] = x3 where...
			// s(x) = x0 + x1*x + x2*x^2 + x3*x^3
			BigDecimal[] constants = new BigDecimal[4];
			
			BigDecimal M_j = mVals.get(i);
			BigDecimal M_k = mVals.get(i-1);
			BigDecimal x_j = new BigDecimal(dataPoints.get(i).x);
			BigDecimal x_k = new BigDecimal(dataPoints.get(i-1).x);
			BigDecimal y_j = new BigDecimal(dataPoints.get(i).y);
			BigDecimal y_k = new BigDecimal(dataPoints.get(i-1).y);
			if(i == 1) {
				M_k = new BigDecimal(0);
			}
			if (i == mVals.size()-1) {
				M_j = new BigDecimal(0);
			}
			
			// Find constants[0] ---> x0
			BigDecimal temp1 = M_k.multiply(x_j.multiply((x_j).multiply(x_j)));
			BigDecimal temp2 = M_j.multiply(x_k.multiply((x_k).multiply(x_k)));
			constants[0] = temp1.subtract(temp2);
			temp1 = y_k.multiply(x_j);
			temp2 = y_j.multiply(x_k);
			BigDecimal temp3 = new BigDecimal(6).multiply(temp1.subtract(temp2));
			constants[0] = constants[0].add(temp3);
			temp1 = x_j.subtract(x_k);
			temp1 = temp1.multiply(temp1);
			temp2 = M_k.multiply(x_j);
			temp3 = M_j.multiply(x_k);
			temp2 = temp2.subtract(temp3);
			constants[0] = constants[0].subtract(temp1.multiply(temp2));
			temp1 = x_j.subtract(x_k);
			constants[0] = constants[0].divide(new BigDecimal(6).multiply(temp1), precision, RoundingMode.HALF_UP);
			
			// Find constants[1] ---> x1
			temp1 = M_k.multiply(x_j.multiply(x_j));
			temp2 = M_j.multiply(x_k.multiply(x_k));
			constants[1] = new BigDecimal(-3).multiply(temp1.subtract(temp2));
			temp1 = new BigDecimal(6).multiply(y_k.subtract(y_j));
			constants[1] = constants[1].subtract(temp1);
			temp1 = x_j.subtract(x_k);
			temp1 = temp1.multiply(temp1);
			temp2 = M_k.subtract(M_j);
			constants[1] = constants[1].add(temp1.multiply(temp2));
			temp1 = x_j.subtract(x_k);
			constants[1] = constants[1].divide((new BigDecimal(6).multiply(temp1)), precision, RoundingMode.HALF_UP);
			
			// Find constants[2] ---> x2
			temp1 = M_k.multiply(x_j);
			temp2 = M_j.multiply(x_k);
			temp3 = new BigDecimal(2).multiply(x_j.subtract(x_k));
			constants[2] = temp1.subtract(temp2);
			constants[2] = constants[2].divide(temp3, precision, RoundingMode.HALF_UP);
			
			// Find constants[3] ---> x3
			constants[3] = M_j.subtract(M_k);
			temp1 = x_j.subtract(x_k);
			constants[3] = constants[3].divide(new BigDecimal(6).multiply(temp1), precision, RoundingMode.HALF_UP);
	
			constantsVec.add(constants);
		}
		
		return constantsVec;
	}
//=================================================================================================
	
	/**
	 * Creates a vector of strings that includes all of the equations (in reverse standard form) 
	 * for every value in the array of BigDecimal values. The following correspond to the
	 * equation below:
	 * 
	 * constantsVec.get(i)[0] = x_0
	 * constantsVec.get(i)[1] = x_1
	 * constantsVec.get(i)[2] = x_2
	 * constantsVec.get(i)[3] = x_3
	 * 
	 * s(x) = x_0 + (x_1 * x) + (x_2 * x^2) + (x_3 * x^3)
	 * 
	 * @param constantsVec  A vector of all arrays of constants for a third degree polynomial
	 * 
	 * @param dataPoints    (x, y) points from a data set in ascending x order. These points NEED to 
	 * 						be the same points used to generate the constantsVec!
	 * 
	 * @return string representations of every approximation function in the required form:
	 * 		   x_k <= x < x_k+1; y_i = c0 + c1x + c2x^2 + c3x^3; interpolation
	 * 		   for each row in the constantsVec vector
	 */
	public static Vector<String> allSplineEqStrings(
			Vector<BigDecimal[]> constantsVec, Vector<Point2D.Double> dataPoints) {
		Vector<String> equations = new Vector<String>();
		
		for(int i=0; i<constantsVec.size(); i++) {
			StringBuilder eqBuilder = new StringBuilder();
			eqBuilder.append("y_" + new Integer(i+1).toString() + " = ");
			for(int j=0; j<constantsVec.get(i).length; j++) {
				String coeffValue = new Double(constantsVec.get(i)[j].doubleValue()).toString();
				if(coeffValue.contains("E")) {
					coeffValue = Utilities.convertEto10(coeffValue);
				}
				eqBuilder.append(coeffValue);
				if(j == 0) {
					eqBuilder.append(" + ");
				}
				else if(j == 1) {
					eqBuilder.append("x + ");
				}
				else {
					eqBuilder.append("x^" + j);
					if(j != constantsVec.get(i).length-1) {
						eqBuilder.append(" + ");
					}
				}
			}
			Integer xStart = new Double(dataPoints.get(i).x).intValue();
			Integer xEnd = new Double(dataPoints.get(i+1).x).intValue();
			equations.add(Utilities.formatEqOutput(xStart, xEnd, new String(eqBuilder), "interpolation"));
		}
		
		return equations;
	}
//=================================================================================================
	
	/**
	 * For each set of four points in a set of data points, find the coefficients between the middle
	 * two points. For the equations between the first two points and the last two points, assume
	 * the M value is 0. This constructs the coefficients for every spline function.
	 * 
	 * This is necessary because using the traditional method takes too much time. For example,
	 * if we have 1200 data points, we would only have one M matrix to solve, but it would be
	 * 1200x1200 entries, and this is massive especially considering that we have to keep 512 
	 * digits of precision for every value in the matrix. By breaking it out into several 4x4 
	 * matrices, this is much more manageable, precise, and MUCH faster!
	 * 
	 * @param dataPoints  Data in (x, y) format in ascending x order. X values should be evenly
	 * 					  spaced apart for the entire data set.
	 * 
	 * @return coeffsVec  A Vector of all coefficients for a cubic equation where,
	 * 					  coeffsVec.get(i)[0] = C_0
	 * 					  coeffsVec.get(i)[1] = C_1
	 * 					  coeffsVec.get(i)[2] = C_2
	 * 					  coeffsVec.get(i)[3] = C_3
	 * 					  in s(x) = C_0*x_0^0 + C_1*x_1^1 + C_2*x_2^2 + C_3*x_3^3 
	 */
	public static Vector<BigDecimal[]> constructSplines(Vector<Point2D.Double> dataPoints) {
		Vector<BigDecimal[]> coeffsVec = new Vector<BigDecimal[]>();
		BigDecimal firstMValue = new BigDecimal(0);
		for(int i=3; i<dataPoints.size(); i++) {
			Vector<Point2D.Double> pointSample = new Vector<Point2D.Double>();
			for(int k=i-3; k<i+1; k++) {
				pointSample.add(dataPoints.get(k));
			}
			AugmentedMatrix mMatrix = CubicSplineUtils.genMMatrix(pointSample);
			mMatrix.setValue(mMatrix.size()-2, mMatrix.get(mMatrix.size()-2).size()-1, firstMValue);
			AugmentedMatrix solved = GaussOps.solve(mMatrix);
			firstMValue = solved.get(0).get(solved.size()-1);
			Vector<BigDecimal[]> sampleCoeffs = CubicSplineUtils.findCoeffs(pointSample, mMatrix);
			if(i == 3) {
				coeffsVec.add(sampleCoeffs.get(0));
			}
			coeffsVec.add(sampleCoeffs.get(1));
			if(i == dataPoints.size()-1) {
				coeffsVec.add(sampleCoeffs.get(2));
			}
		}
		
		return coeffsVec;
	}
//=================================================================================================	
	
}
