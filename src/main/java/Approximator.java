/**
 * This program will take a data file described in the documentation for the main function and
 * determine two types of approximation functions. Each column in a data file will have
 * it's own output file. Firstly, there will be a least squares approximation function for 
 * every point in a column. This can be any polynomial from degree 0 to infinity (theoretically). 
 * It will output that equation to the data file. What follows is a list of interpolation 
 * approximation functions between each set of x values. If there are N points in the data file, 
 * there will be only one least squares approximation function, while there will be N-1 
 * interpolation functions (either linear or cubic-spline depending on preference).
 * 
 * @author  Alexander Wojtowicz
 * @version 1.0
 * @since   2019-04-11
 */

import java.awt.geom.Point2D;
import java.io.File;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.Vector;

public class Approximator {

	/** 
	 * Data files should be in the following format:
	 * 
	 *		y11  y12  y13 ... y1M
	 *		y21  y22  y23 ... y2M
	 *		y31  y32  y33 ... y3M
	 *		.    .    .    .    . 
	 *		.    .    .    .    . 
	 *		.    .    .    .    .
	 *		yN1  yN2  yN3 ... yNM
	 *      
	 * Notice how there are no x values. Each row of y's corresponds to a predetermined x value.
	 * Ideally, these x values represent periodic time stamps over consistent intervals. These
	 * intervals must be known beforehand, and this is one of the parameters described below. The
	 * first x value begins at x=0.
	 * 
	 * The output files will be created for each column in the input file. Inside, 
	 * there will be a least-squares approximation function and N-1 interpolation functions over 
	 * every domain between two points.
	 * 
	 * Inside of the newly created directory for the output files will be a 'graphs' directory.
	 * Inside, there will be a graph for each file generated in the output.
	 *                                          
	 * @param args  A string array holding the command line arguments:
	 * 		args[0] == <File> absolute/relative path of data file
	 * 		args[1] == <Integer> time interval between each line in the data file
	 * 		args[2] == <Integer> degree of polynomial for least squares approximation
	 * 		args[3] == <String> either 'linear' or 'cubic'. Interpolation type 
	 */
	public static void main(String[] args) {
		if(args.length != 4) {
			Utilities.printUsageError();
		}
		else {
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
			LocalDateTime now = LocalDateTime.now();
			String begin = dtf.format(now).toString().split("\\s+")[1];
			try {
				File dataFile = new File(args[0]);
				int xInterval = Integer.parseInt(args[1]);
				int polyDegree = Integer.parseInt(args[2]);
				String interpolFunc = new String();
				if(args[3].equals("linear") || args[3].equals("cubic")) {
					interpolFunc = args[3];
				}
				else {
					throw new Exception("args[3] must equal 'linear' OR 'cubic'!");
				}
				System.out.println("*****");
				Vector<String> equations = new Vector<String>();
				LinkedHashMap<Integer, Vector<Double>> dataSet = 
						Utilities.populateDataSet(dataFile.getCanonicalPath().toString(), xInterval);
				String outFileName = Utilities.getRawFileName(dataFile);
				File outDir = new File(dataFile.getParentFile().getCanonicalPath() + 
						System.getProperty("file.separator") + outFileName + "-output");
				boolean wasCreated = outDir.mkdir();
				if(wasCreated) {
					System.out.println("Created Directory : " + 
							outDir.toString() + 
							System.getProperty("file.separator"));
				}
				else {
					System.out.println("Directory Exists  : " +
							outDir.toString() +
							System.getProperty("file.separator"));
				}
				new File(dataFile.getParentFile().getCanonicalPath() + 
						System.getProperty("file.separator") + outFileName + "-output" +
						System.getProperty("file.separator") + "graphs").mkdir();
				
				File dataSetFile = new File(outDir + 
						System.getProperty("file.separator") + 
						outFileName + 
						"-DATA.txt");
				PrintWriter dwriter = new PrintWriter(dataSetFile);
				dwriter.write(Utilities.dataSetToString(dataSet));
				dwriter.close();
				
				for(int i=0; i<dataSet.get(0).size(); i++) {
					Matrix x   = LeastSquaresUtils.getXMatrixForPolynomial(dataSet, polyDegree);
					Matrix xT  = new Matrix(MatrixOps.transpose(x.getMatrix()));
					Matrix y   = LeastSquaresUtils.getYMatrix(dataSet, i);
					Matrix xTx = MatrixOps.dotProduct(xT, x); 
					Matrix xTy = MatrixOps.dotProduct(xT, y);
					
					AugmentedMatrix a = new AugmentedMatrix(xTx, xTy);
					GaussOps.solve(a);
					String approxEq = LeastSquaresUtils.approxEq(a, 0);
					equations.add(Utilities.formatEqOutput(
							0, 
							(dataSet.size()*xInterval)-xInterval, 
							approxEq, 
							"least-squares"));
					
					if(interpolFunc.equals("linear")) {
						int counter = 0;
						for(int j=0; j<dataSet.size(); j++) {
							if(j != 0) {
								Point2D.Double p1 = new Point2D.Double(
									new Double(counter-xInterval), 
									dataSet.get(counter-xInterval).get(i));
								Point2D.Double p2 = new Point2D.Double(
									new Double(counter),
									dataSet.get(counter).get(i));
								String eq = LinearSplineUtils.linearEqToString(
									LinearSplineUtils.findLinearEq(p1, p2), j);
							
								equations.add(Utilities.formatEqOutput(
									counter-xInterval, 
									counter, 
									eq, 
									"interpolation"));
							}
							counter += xInterval;
						}
					}
					else {
						Vector<Point2D.Double> points = Utilities.getPointsAtIndex(dataSet, i);
						Vector<BigDecimal[]> constants = CubicSplineUtils.constructSplines(points);
						Vector<String> splines = CubicSplineUtils.allSplineEqStrings(constants, points);
						for(int j=0; j<splines.size(); j++) {
							equations.add(splines.get(j));
						}
					}
					String outFileDir = dataFile.getParentFile().getCanonicalPath();
					String outFile = outFileDir + 
							System.getProperty("file.separator") + 
							outFileName + "-output" + 
							System.getProperty("file.separator") +
							outFileName + "-output_" + i + ".txt";
				
					Utilities.printDataToFile(equations, outFile);
					equations.clear();
					Grapher grapher = new Grapher(new File(outFile), dataSetFile);
					File graphFile = new File(outFileDir + 
							System.getProperty("file.separator") + 
							outFileName + "-output" +
							System.getProperty("file.separator") +
							"graphs" +
							System.getProperty("file.separator") +
							outFileName + "-output_" + i + ".png");
					grapher.printGraph(Utilities.getRawFileName(graphFile), graphFile);
				}
				System.out.println("*****");
				now = LocalDateTime.now();
				String end = dtf.format(now).toString().split("\\s+")[1];
				System.out.println("FINISHED - " + Utilities.timeElapsed(begin, end));
			}
			catch(Exception e) {
				System.out.println("Error:  Something went wrong...");
				//Utilities.printUsageError();
				e.printStackTrace();
			}
		}
	}
//=================================================================================================
	
}
