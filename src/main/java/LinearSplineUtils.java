/**
 * A collection of functions dealing with linear interpolation. Linear interpolation refers to 
 * the finding of a function for a line that passes through two points.
 * 
 * @author  Alexander Wojtowicz
 * @version 1.0
 * @since   2019-04-11
 */

import java.awt.geom.Point2D;

public class LinearSplineUtils {
	
	public LinearSplineUtils() {}
//=================================================================================================
	
	/**
	 * Given two points, this finds the slope and y-intercept that would create a line between them of 
	 * the form f(x) = mx + b, where m is the slope and b is the y-intercept.
	 * 
	 * m = (y2 - y1)/(x2 - x1)
	 * b = y1 - m*x1
	 * 
	 * @param point1 (x1, y1)
	 * @param point2 (x2, y2)
	 * 
	 * @return a "point" where the x position is the slope and the y position is the y intercept. 
	 * 		   In actuality, this is a pair and not a point.
	 */
	public static Point2D.Double findLinearEq(Point2D.Double point1, Point2D.Double point2) {
		double slope = (point2.y - point1.y) / (point2.x - point1.x);
		double yint = point1.y - (slope * point1.x);
		
		return new Point2D.Double(slope, yint);
	}	
//=================================================================================================
	
	/**
	 * Given the slope and y-intercept, return a string containing the equation of a line
	 * between two specified points.
	 * 
	 * @param params  A Point2D.Double where params.x = slope and params.y = y-intercept
	 * @param num	  The subscript attached to the LHS, i.e., y_0 = mx + b; num = 0
	 * 
	 * @return string representation of the equation of a line 
	 */
	public static String linearEqToString(Point2D.Double params, Integer num) {
		StringBuilder builder = new StringBuilder();
		builder.append("y_");
		builder.append(num.toString());
		builder.append(" = ");
		if(new Double(params.y).toString().contains("E")) {
			builder.append(Utilities.convertEto10(new Double(params.y).toString()));
		}
		else {
			builder.append(params.y);
		}
		builder.append(" + ");
		if(new Double(params.x).toString().contains("E")) {
			builder.append(Utilities.convertEto10(new Double(params.x).toString()));
		}
		else {
			builder.append(params.x);
		}
		builder.append("x");
		
		return new String(builder);
	}
//=================================================================================================
	
}
