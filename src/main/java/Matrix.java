/** 
 * A Matrix is a structure with N rows and M columns. All columns in every row are the same length.
 * These matrices are NOT augmented, meaning the GaussOps methods are not compatible.
 * 
 * @author  Alexander Wojtowicz
 * @version 1.0
 * @since   2019-04-11
 */

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.Vector;
import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;

public class Matrix {
	
	private final int RAND_VAL_LOWER_BOUND = -25;
	private final int RAND_VAL_UPPER_BOUND = 25;
	private final int RAND_LEN_LOWER_BOUND = 2;
	private final int RAND_LEN_UPPER_BOUND = 7;
	private Vector<Vector<BigDecimal>> matrix;
//=================================================================================================
	
	/** 
	 * Generates a random Matrix (no parameters specified).
	 */
	public Matrix() {
		this.matrix = new Vector<Vector<BigDecimal>>();
		int valRange = RAND_VAL_UPPER_BOUND - RAND_VAL_LOWER_BOUND + 1;
		int lenRange = RAND_LEN_UPPER_BOUND - RAND_LEN_LOWER_BOUND + 1;
		int rowLength = (int)(Math.random() * lenRange) + RAND_LEN_LOWER_BOUND;
		int colLength = rowLength + 1;
		
		for(int i=0; i<rowLength; i++) {
			Vector<BigDecimal> row = new Vector<BigDecimal>();
			for(int j=0; j<colLength; j++) {
				// 20% chance of being 0
				if((int)(Math.random() * 5) == 0) {
					row.add(new BigDecimal(0));
				}
				else {
					Double val = (double)(Math.random() * valRange) + RAND_VAL_LOWER_BOUND;
					String v = new String();
					// 50% chance of being an 'integer'
					if((int)(Math.random() * 2) == 0) {
						v = String.format("%.0f", val);
					}
					else {
						v = String.format("%.3f", val);
					}
					row.add(new BigDecimal(Double.parseDouble(v)));
				}
			}
			this.matrix.add(row);
			//System.out.println(row);
		}
	}
//=================================================================================================
	
	/**
	 * Generates a matrix from a file. The file has been checked for format previously.
	 * 
	 * @param file containing the matrix
	 * 
	 * @throws FileNotFoundException if specified file does not exist
	 */
	public Matrix(File file) throws FileNotFoundException {
		this.matrix = new Vector<Vector<BigDecimal>>();
		Vector<String[]> lines = new Vector<String[]>();
		
		Scanner reader = new Scanner(file);
		while(reader.hasNextLine()) {
			String line = reader.nextLine();
			lines.add(line.split("\\s+"));
		}
		reader.close();

		for(String[] row : lines) {
			Vector<BigDecimal> rowVec = new Vector<BigDecimal>();
			for(String elem : row) {
				rowVec.add(new BigDecimal(Double.parseDouble(elem)));
			}
			this.matrix.add(rowVec);
		}
	}
//=================================================================================================
	
	/**
	 * Loads a matrix from a Vector<Vector<BigDecimal>>. The outer vector is the row, and the inner
	 * vector is the column. If not all the columns are the same length, then the Matrix::matrix
	 * will be left empty.
	 * 
	 * @param a matrix-like structure with NxM dimensions (Vector<Vector<BigDecimal>>) 
	 */
	public Matrix(Vector<Vector<BigDecimal>> mat) {
		this.matrix = new Vector<Vector<BigDecimal>>();
		
		Set<Integer> colWidths = new HashSet<Integer>();
		for(int i=0; i<mat.size(); i++) {
			colWidths.add(mat.get(i).size());
		}
		if(colWidths.size() == 1) {
			for(int i=0; i<mat.size(); i++) {
				Vector<BigDecimal> row = new Vector<BigDecimal>();
				for(int j=0; j<mat.get(i).size(); j++) {
					row.add(mat.get(i).get(j));
				}
				this.matrix.add(row);
			}
		}
		else {
			System.out.println("Error:  Could not generate Matrix. Variable column widths!");
		}
	}
//=================================================================================================
	
	/**
	 * Gets the Matrix::matrix structure (Vector<Vector<Double>>).
	 * 
	 * @return matrix
	 */
	public Vector<Vector<BigDecimal>> getMatrix() {
		return this.matrix;
	}
//=================================================================================================
	
	/**
	 * Gets the number of rows in the Matrix::matrix.
	 * 
	 * @return number of rows in matrix 
	 */
	public int size() {
		return this.matrix.size();
	}
//=================================================================================================
	
	/**
	 * Gets the value at some index of the Matrix::matrix.
	 * Precondition:  rowIndex and colIndex are in bounds.
	 * 
	 * @param rowIndex
	 * @param colIndex
	 * 
	 * @return The value at this index:  (rowIndex, colIndex) 
	 */
	public BigDecimal get(int rowIndex, int colIndex) {
		return this.matrix.get(rowIndex).get(colIndex);
	}
//=================================================================================================
	
	/**
	 * Gets the row at some index in the Matrix::matrix.
	 * Precondition:  rowIndex is within Matrix::matrix.size().
	 * 
	 * @param rowIndex
	 * 
	 * @return Vector<BigDecimal> (the row at some rowIndex)
	 */
	public Vector<BigDecimal> getRow(int rowIndex) {
		return this.matrix.get(rowIndex);
	}
//=================================================================================================
	
	/**
	 * Gets the column at some index (a Vector<BigDecimal>).
	 * Precondition:  colIndex is within Matrix::matrix.size().
	 * 
	 * @param colIndex
	 * 
	 * @return Vector<BigDecimal> (the column at some colIndex)
	 */
	public Vector<BigDecimal> getCol(int colIndex) {
		Vector<Vector<BigDecimal>> tMatrix = MatrixOps.transpose(this.matrix);
		return tMatrix.get(colIndex);
	}
//=================================================================================================
	
	/**
	 * String representation of the (formatted) Matrix.
	 * 
	 * @return Matrix::matrix in string form
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		Vector<Vector<BigDecimal>> tMatrix = MatrixOps.transpose(this.matrix);
		int[] maxColLens = new int[tMatrix.size()];
		
		for(int i=0; i<tMatrix.size(); i++) {
			int max = 0;
			for(int j=0; j<tMatrix.get(i).size(); j++) {
				String elem = (tMatrix.get(i).get(j)).toString();
				if(elem.length() > max) {
					max = elem.length();
				}
			}
			maxColLens[i] = max;
		}
		
		for(int i=0; i<this.matrix.size(); i++) {
			for(int j=0; j<this.matrix.get(i).size(); j++) {
				if(j != 0) {
					builder.append("  ");
				}
				String elem = (this.matrix.get(i).get(j)).toString();
				int numSpaces = maxColLens[j] - elem.length();
				for(int k=0; k<numSpaces; k++) {
					builder.append(" ");
				}
				builder.append(this.matrix.get(i).get(j));
			}
			builder.append("\n");
		}
		
		return new String(builder);
	}
//=================================================================================================
	
	/**
	 * Sets the value at some index (rowIndex x colIndex).
	 * 
	 * @param rowIndex
	 * @param colIndex
	 * @param value
	 */
	public void setValue(int rowIndex, int colIndex, BigDecimal value) {
		this.matrix.get(rowIndex).remove(colIndex);
		this.matrix.get(rowIndex).add(colIndex, value);
	}
//=================================================================================================
	
}
