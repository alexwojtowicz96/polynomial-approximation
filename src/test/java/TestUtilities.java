/**
 * @author Alexander Wojtowicz
 */

import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import static org.junit.Assert.*;

public class TestUtilities {
	
	String time1 = "10:15:00";
	String time2 = "10:45:02";
	String time3 = "21:11:52";
//=================================================================================================
	
	/**
	 * 
	 */
	@Before
	public void setup() {
		
	}
	
	@After
	public void after() {

	}
//=================================================================================================
	
	/**
	 * 
	 */
	@Test
	public void testTimeElapsed() {
		System.out.println(Utilities.timeElapsed(time1, time2));
		System.out.println(Utilities.timeElapsed(time1, time3));
		assertTrue(Utilities.timeElapsed(time1, time2).equals(new String("[00]HOURS [30]MIN [02]SEC")));
		assertTrue(Utilities.timeElapsed(time1, time3).equals(new String("[10]HOURS [56]MIN [52]SEC")));
	}
//=================================================================================================
	
}
