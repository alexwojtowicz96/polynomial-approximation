/**
 * This is not a true test class. It is merely some code to help visualize some of the steps after
 * certain methods finish executing.
 * 
 * @author Alexander Wojtowicz
 */

import java.math.BigDecimal;
import java.util.Vector;
import org.junit.Test;
import org.junit.Before;
//import org.junit.After;
//import org.junit.Ignore;
//import static org.hamcrest.CoreMatchers.*;
//import static org.junit.Assert.*;

public class TestGaussOps {
	
	AugmentedMatrix aMatrix;
	Matrix a;
	Matrix b;
//=================================================================================================
	
	@Before
	/**
	 * aMatrix<AugmentedMatrix> = a<Matrix> | b<Matrix>
	 * 
	 * 0.333  1.0    0.167  0.0    0.0    |   2.5
	 * 0.0    0.167  1.0    0.333  0.0    |  -8.5
	 * 0.0    0.0    0.333  2.333  0.833  |   2.1
	 * 1.0    0.0    0.0    0.0    0.0    |   0.0
	 * 0.0    0.0    0.0    0.0    1.0    |   0.0  
	 */
	public void setup() {
		Vector<Vector<BigDecimal>> mat = new Vector<Vector<BigDecimal>>();
		Vector<BigDecimal> row = new Vector<BigDecimal>();
		row.add(new BigDecimal(1/3));
		row.add(new BigDecimal(1));
		row.add(new BigDecimal(1/6));
		row.add(new BigDecimal(0));
		row.add(new BigDecimal(0));
		mat.add(row);
		
		row = new Vector<BigDecimal>();
		row.add(new BigDecimal(0));
		row.add(new BigDecimal(1/6));
		row.add(new BigDecimal(1));
		row.add(new BigDecimal(1/3));
		row.add(new BigDecimal(0));
		mat.add(row);
		
		row = new Vector<BigDecimal>();
		row.add(new BigDecimal(0));
		row.add(new BigDecimal(0));
		row.add(new BigDecimal(1/3));
		row.add(new BigDecimal(7/3));
		row.add(new BigDecimal(5/6));
		mat.add(row);
		
		row = new Vector<BigDecimal>();
		row.add(new BigDecimal(1));
		row.add(new BigDecimal(0));
		row.add(new BigDecimal(0));
		row.add(new BigDecimal(0));
		row.add(new BigDecimal(0));
		mat.add(row);
		
		row = new Vector<BigDecimal>();
		row.add(new BigDecimal(0));
		row.add(new BigDecimal(0));
		row.add(new BigDecimal(0));
		row.add(new BigDecimal(0));
		row.add(new BigDecimal(1));
		mat.add(row);
		
		a = new Matrix(mat);
		row = new Vector<BigDecimal>();
		mat = new Vector<Vector<BigDecimal>>();
		row.add(new BigDecimal(5/2));
		mat.add(row);
		row = new Vector<BigDecimal>();
		row.add(new BigDecimal(-8.5));
		mat.add(row);
		row = new Vector<BigDecimal>();
		row.add(new BigDecimal(21/10));
		mat.add(row);
		row = new Vector<BigDecimal>();
		row.add(new BigDecimal(0));
		mat.add(row);
		row = new Vector<BigDecimal>();
		row.add(new BigDecimal(0));
		mat.add(row);
		b = new Matrix(mat);
		
		aMatrix = new AugmentedMatrix(a, b);
	}
//=================================================================================================
	
	/**
	 * aMatrix after being solved should look like...
	 * 
	 * 1.0  0.0  0.0  0.0  0.0  |     0.0
	 * 0.0  1.0  0.0  0.0  0.0  |   4.161
	 * 0.0  0.0  1.0  0.0  0.0  |  -9.968
	 * 0.0  0.0  0.0  1.0  0.0  |   2.324
	 * 0.0  0.0  0.0  0.0  1.0  |     0.0
	 */
	@Test
	public void testSolve() {
		System.out.println(new BigDecimal(new Double(1) / new Double(3)));
		System.out.println("ORIGINAL");
		System.out.print(aMatrix.toString());
		aMatrix = GaussOps.solve(aMatrix);
		System.out.println("\nSOLVED");
		System.out.print(aMatrix.toString());
	}
//=================================================================================================
	
}
