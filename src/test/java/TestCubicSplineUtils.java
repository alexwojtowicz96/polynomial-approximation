/**
 * This is not a true test class. It is merely some code to help visualize some of the steps after
 * certain methods finish executing.
 * 
 * @author Alexander Wojtowicz
 */

import java.awt.geom.Point2D;
import java.io.File;
import java.io.IOException;
//import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Vector;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
//import org.junit.Ignore;
//import static org.junit.Assert.*;
//import static org.hamcrest.CoreMatchers.*;

public class TestCubicSplineUtils {

	File dataSample;
	File sensorsShort;
	LinkedHashMap<Integer, Vector<Double>> dataSet;
	LinkedHashMap<Integer, Vector<Double>> dataSet2;
	Vector<Point2D.Double> points1;
	Vector<Point2D.Double> points2;
	Vector<Point2D.Double> points3;
//=================================================================================================
	
	/**
	 * @throws IOException
	 */
	@Before
	public void setup() throws IOException {
		dataSample = new File("src" + 
				System.getProperty("file.separator") +
				"test" + 
				System.getProperty("file.separator") +
				"resources" + 
				System.getProperty("file.separator") +
				"data-sample.txt");
		sensorsShort = new File("src" + 
				System.getProperty("file.separator") +
				"test" + 
				System.getProperty("file.separator") +
				"resources" + 
				System.getProperty("file.separator") +
				"sensors-short.txt");
		dataSet = Utilities.populateDataSet(dataSample.getCanonicalPath(), 30);
		dataSet2 = Utilities.populateDataSet(sensorsShort.getCanonicalPath(), 30);
		points1 = Utilities.getPointsAtIndex(dataSet, 0);
		points3 = Utilities.getPointsAtIndex(dataSet2, 2);
		
		points2 = new Vector<Point2D.Double>();
		points2.add(new Point2D.Double(new Double(2), new Double(1)));
		points2.add(new Point2D.Double(new Double(4), new Double(6)));
		points2.add(new Point2D.Double(new Double(5), new Double(11)));
		points2.add(new Point2D.Double(new Double(7), new Double(4)));
		points2.add(new Point2D.Double(new Double(12), new Double(-3)));
	}
	
	@After
	public void after() {
		System.out.println("\n");
	}
//=================================================================================================
	
	/**
	 * 
	 */
	@Test
	public void testGetMMatrix() {
		for(int i=0; i<points1.size(); i++) {
			System.out.println("i=" + i + " : " + "(" + points1.get(i).x + ", " + points1.get(i).y + ")");
		}
		AugmentedMatrix aMatrix = CubicSplineUtils.genMMatrix(points1);
		System.out.println("\nORIGINAL");
		System.out.print(aMatrix.toString());
		aMatrix = GaussOps.solve(aMatrix);
		System.out.println("\nSOLVED");
		System.out.print(aMatrix.toString());
	}
//=================================================================================================
	
	/**
	 * 
	 */
	/*
	@Test
	public void testFindCoeffs() {
		Vector<BigDecimal[]> constants = CubicSplineUtils.findCoeffs(points1);
		System.out.println("CONSTANTS ARRAYS ***");
		for(int i=0; i<constants.size(); i++) {
			for(int j=0; j<constants.get(i).length; j++) {
				System.out.print(constants.get(i)[j] + " ");
			}
			System.out.println();
		}
	}*/
//=================================================================================================
	
	/**
	 * 
	 */
	/*
	@Test
	public void testAllSplineEqStrings() {
		System.out.println("ALL CUBIC SPLINE EQUATION OUTPUTS");
		Vector<BigDecimal[]> constants = CubicSplineUtils.findCoeffs(points1);
		Vector<String> equations = CubicSplineUtils.allSplineEqStrings(constants, points1);
		for(int i=0; i<equations.size(); i++) {
			System.out.println(equations.get(i));
		}
	}*/
//=================================================================================================
	
}
