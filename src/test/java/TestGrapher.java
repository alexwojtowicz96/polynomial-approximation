/**
 * @author Alexander Wojtowicz
 */

import java.math.BigDecimal;
import java.util.Vector;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class TestGrapher {
	
	final String SLASH = System.getProperty("file.separator");
	File equationFile;
	File dataSetFile;
	Grapher grapher;
//=================================================================================================
	
	/**
	 * @throws IOException 
	 * @throws NumberFormatException 
	 */
	@Before
	public void setup() throws NumberFormatException, IOException {
		equationFile = new File("src" + SLASH + "main" + SLASH + "resources" + SLASH + 
				"sensors-data-CUBIC" + SLASH + "sensors-2018.12.26-output" + SLASH + 
				"sensors-2018.12.26-output_0.txt");
		dataSetFile = new File("src" + SLASH + "main" + SLASH + "resources" + SLASH + 
				"sensors-data-CUBIC" + SLASH + "sensors-2018.12.26-output" + SLASH + 
				"sensors-2018.12.26-DATA.txt");
		grapher = new Grapher(equationFile, dataSetFile);
	}
	
	@After
	public void after() {
		System.out.println();
	}
//=================================================================================================
	
	/**
	 * 
	 */
	@Test
	public void testDisplay() {
		System.out.println("POINTS");
		for(int i=0; i<grapher.getPoints().size(); i++) {
			System.out.println("(" + grapher.getPoints().get(i).x + ", " + grapher.getPoints().get(i).y + ")");
		}
		System.out.println("\nEQUATIONS & DOMAINS");
		for(Vector<BigDecimal> eq : grapher.getLeastSquaresEqs().keySet()) {
			System.out.println(eq + " : " + grapher.getLeastSquaresEqs().get(eq)[0].toString() + 
					" to " + grapher.getLeastSquaresEqs().get(eq)[1].toString());
		}
		for(Vector<BigDecimal> eq : grapher.getInterpolationEqs().keySet()) {
			System.out.println(eq + " : " + grapher.getInterpolationEqs().get(eq)[0].toString() + 
					" to " + grapher.getInterpolationEqs().get(eq)[1].toString());
		}
	}
//=================================================================================================
	
}
