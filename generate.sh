# Exectute './gradlew makeJar' first

#!/bin/bash
ERRORMSG='   You must supply a FILE and interpolation TYPE at the command line'
if [ $# -ne 2 ] ; then
	echo $ERRORMSG
	echo '   You must supply two parameters!'
else
	FILE=$1  # Name of valid input file
	TYPE=$2  # Interpolation type (linear or cubic-spline)
	if [ -f $FILE ] ; then
		if [[ ( $TYPE == 'cubic' ) || ( $TYPE == 'linear' ) ]] ; then
			XINT='30'   # Interval between each x value (starting at x=0)
			DEGREE='1'  # Degree of the polynomial for least-squares approximation
			java -jar build/libs/Approximate-1.0.jar $FILE $XINT $DEGREE $TYPE
		else
			echo $ERRORMSG
			echo "   Type '$TYPE' must be 'linear' OR 'cubic'!"
		fi
	else
		echo $ERRORMSG
		echo "   File '$FILE' not found!"
		if [[ ( $TYPE != 'cubic' ) && ( $TYPE != 'linear' ) ]] ; then
			echo "   Type '$TYPE' must be 'linear' OR 'cubic'!"
		fi
	fi
fi